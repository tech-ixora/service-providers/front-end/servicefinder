FROM nginx:alpine-slim
ARG CI_PROJECT_URL
RUN apk update --no-cache && apk upgrade --no-cache


COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf



#COPY --from=node /app/target/classes/static /usr/share/nginx/html
########### Attention copy all (.) will expose sensitive data like .git directory and source codes
############COPY . /usr/share/nginx/html

COPY . /usr/share/nginx/html
#ADD assets /usr/share/nginx/html/assets


RUN mkdir -p /var/cache/nginx/ && chown -R nginx:nginx /etc/nginx/ /var/cache/nginx /usr/share/nginx/
RUN ls -la /usr/share/nginx/html/


EXPOSE 8888
CMD ["nginx", "-g", "daemon off;"]
